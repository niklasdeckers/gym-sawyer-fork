sudo make build-nvidia-sawyer-sim
sudo make run-nvidia-sawyer-sim
( To exit the container, type sudo docker stop sawyer-sim in a new terminal )

run this command in a new terminal:

sudo make run-garage-nvidia-ros RUN_CMD="bash"

then, run the following commands in the same terminal:

cd /root && \
  [ -d Lasagne ] || { \
    git clone https://github.com/Lasagne/Lasagne.git && \
    cd Lasagne && \
    git reset --hard a61b76fd991f84c50acdb7bea02118899b5fefe1 \
  ; } && \
  cd /root/Lasagne/ && \
  python3 -m pip install -r requirements.txt && \
  python3 -m pip install . && cd /root/code/gym-sawyer

python3 sawyer/ros/envs/sawyer/launchers/trpo_sim_sawyer_pnp.py

python3 sawyer/ros/envs/sawyer/launchers/trpo_sim_sawyer_pnp.py

